# capsulecrm-launcher

Launch and authenticate to your CapsuleCRM space without any user interaction.

# Requirement

You need to have Chrome installed.

Tested on macOS and Windows, available for Linux but not tested.

# Building

Simply run `script/build`. The binary artifacts will be generated in `bin/[macos|linux|windows]`

# Using

```
Usage of ./capsulecrm-launcher:
  -account string
    	CapsuleCRM Account name
  -password string
    	CapsuleCRM user account password
  -username string
    	CapsuleCRM user account name
```

So simply provide the CapsuleCRM subdomain (or account name), account name and password.

Once the user logs-out the Chrome instance is closed.

# License

Copyright Andre Courchesne - Consultant. Licensed under the [MIT license](/LICENSE?raw=true).
