package main

import (
	"flag"
	"log"
	"time"

	"github.com/sclevine/agouti"
)

func main() {

	// Process the command line arguments
	crmAccount := flag.String("account", "", "CapsuleCRM Account name")
	crmUserName := flag.String("username", "", "CapsuleCRM user account name")
	crmPassword := flag.String("password", "", "CapsuleCRM user account password")
	flag.Parse()

	// Exit if we don't have the minimum required
	if *crmAccount == "" || *crmUserName == "" || *crmPassword == "" {
		log.Fatal("Missing some required arguments...")
	}

	// Open the Chrome driver
	driver := agouti.ChromeDriver()
	if err := driver.Start(); err != nil {
		log.Fatal("Failed to start driver:", err)
	}

	// Start a new browser page
	page, err := driver.NewPage()
	if err != nil {
		log.Fatal("Failed to open page:", err)
	}

	// Go to the CapsuleCRM login page
	if err := page.Navigate("https://" + *crmAccount + ".capsulecrm.com/login"); err != nil {
		log.Fatal("Failed to navigate:", err)
	}

	// Enter the credentials and login
	page.FindByID("login:usernameDecorate:username").Fill(*crmUserName)
	page.FindByID("login:passwordDecorate:password").Fill(*crmPassword)
	page.FindByID("login:login").Click()

	// Spin till the window is closed or the user logsout
	gone := false
	for !gone {
		time.Sleep(2 * time.Second)
		nameObject := page.FirstByClass("nav-bar-username")
		name, _ := nameObject.Text()

		if name == "" {
			gone = true
		}
	}

	// Terminate the terminal
	if err := driver.Stop(); err != nil {
		log.Fatal("Failed to close pages and stop WebDriver:", err)
	}

}
